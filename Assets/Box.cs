﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    public int hp = 3;
    public GameObject particles;

    public AudioClip exlodesound;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Damage"))
        {
            hp--;
        }

        if (hp <= 0)
        {

            GameObject g = new GameObject("asfa", typeof(AudioSource));
            g.GetComponent<AudioSource>().PlayOneShot(exlodesound);

            Destroy(g, 5);

            Instantiate(particles, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
