﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    //Скорость движения
    public float speed = 3;
    //Сила прыжка
    public float jumpForce = 10;
    //Объект пули что создовать
    public GameObject bullet;

    public AudioClip jumpSound;
    public AudioClip shootSound;

    //Компонент rigidbody
    Rigidbody2D rb;
    Animator anim;
    AudioSource playerAudio;

    void Start()
    {
        //Получить компонент Rigidbody2D из себя
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
    }

    void Update()
    {
        //Получить горизонтальное передвижение (от -1 до 1) и умножить его на скорость
        float move = Input.GetAxis("Horizontal") * speed;

        //Задаем скорость объекта по Х оси, а по Y остовляем как есть
        rb.velocity = new Vector2(move, rb.velocity.y);

        //Если нажали на кнопку Jump то добавить ссилы в напровление вверх
        if (Input.GetButtonDown("Jump"))
        {
            rb.AddForce(Vector2.up * jumpForce);
            anim.SetTrigger("Jump");

            playerAudio.PlayOneShot(jumpSound);
        }
        
        if(Input.GetButtonDown("Fire1"))
        {
            playerAudio.PlayOneShot(shootSound);

            Vector3 mousePos = Input.mousePosition;
            Vector3 pos = Camera.main.ScreenToWorldPoint(mousePos);
            Vector3 direction = pos - (transform.position + Vector3.up * 0.7f);
            
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;


            //Создать пулю, на позиции игрока, с вращением пули
            Instantiate(bullet, transform.position + Vector3.up * 0.7f, Quaternion.AngleAxis(angle, Vector3.forward));
        }
    }
}
